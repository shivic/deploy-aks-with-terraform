####################################################################################################

# AZURE PROPERTIES

####################################################################################################

variable "location" {
  default = "eastus"
}

####################################################################################################

# NAMING IDENTIFIERS 

####################################################################################################

variable "environment" {
  default = "default"
}

variable "application_name" {
  default = "aks-cluster"
}

variable "department_name" {
  default = "cto"
}

variable "project_name" {
  default = "dncpsf"
}

variable "cost_center" {
  default = "12345678"
}

####################################################################################################

# AGENT POOL CONFIGURATION

####################################################################################################

variable "ssh_public_key" {
  default = "~/.ssh/id_rsa.pub"
}

variable "agent_count" {
  default = 1
}

variable "vm_size" {
  default = "Standard_D1_v2"
}

variable "os_type" {
  default = "Linux"
}

variable "os_disk_size_gb" {
  default = 30
}
