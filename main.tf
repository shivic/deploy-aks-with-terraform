####################################################################################################

# PROVIDERS

# Terraform interpolation is not supported for the special version arguments (highlighted).

# They must be hard coded in.

####################################################################################################

provider "azurerm" {
  version         = "~> 1.5.0"
  subscription_id = "${var.arm_subscription_id}"
  client_id       = "${var.arm_app_id}"
  client_secret   = "${var.arm_password}"
  tenant_id       = "${var.arm_tenant}"
}

####################################################################################################

# RESOURCES

####################################################################################################

resource "azurerm_resource_group" "k8s" {
  name     = "${var.environment}-${var.application_name}-rg"
  location = "${var.location}"
}

resource "azurerm_kubernetes_cluster" "k8s" {
  name                = "${var.application_name}"
  dns_prefix          = "${var.environment}-${var.application_name}-${var.department_name}-${var.project_name}"
  resource_group_name = "${azurerm_resource_group.k8s.name}"
  location            = "${azurerm_resource_group.k8s.location}"

  linux_profile {
    admin_username = "ubuntu"

    ssh_key {
      key_data = "${file("${var.ssh_public_key}")}"
    }
  }

  agent_pool_profile {
    name            = "${var.environment}"
    count           = "${var.agent_count}"
    vm_size         = "${var.vm_size}"
    os_type         = "${var.os_type}"
    os_disk_size_gb = "${var.os_disk_size_gb}"
  }

  service_principal {
    client_id     = "${var.arm_app_id}"
    client_secret = "${var.arm_password}"
  }

  tags {
    Environment = "${var.environment}"
    Department  = "${var.department_name}"
    Project     = "${var.project_name}"
    CostCenter  = "${var.cost_center}"
  }
}
