Deploy an AKS cluster using Terraform
=====================================

This repository will create a working Kubernetes cluster on Azure using Terraform and AKS, then deploy a Kubernetes application with the Kubernetes manifest (`wordsmith-demo.yml`) using images from a remote repository ([Docker Hub](https://hub.docker.com/)).

Pre-requisites
--------------

* Azure subscription
* [Terraform](https://www.terraform.io/intro/getting-started/install.html) 
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

Steps to get started
--------------------

1.  Edit the secrets.tf.template file to have the correct Azure credentials for full access.
    *  e.g. `$ cp secrets.tf.template secrets.tf && nano secrets.tf`
2.  Add the correct SSH key path in variables.tf. Default is ~/.ssh/id_rsa.pub.
3.  Deploy the necessary infrastructure components using Terraform.
    *  `$ terraform init`
    *  (Optional) `$ terraform plan`
    *  `$ terraform apply`
4.  Enter yes at the prompt.
5.  Configure Kubernetes config for kubectl:
    *  `$ echo "$(terraform output kube_config)" > ~/.kube/azurek8s`
    *  `$ export KUBECONFIG=~/.kube/azurek8s`
6.  Check cluster health:
    *  `$ kubectl get nodes`
7.  Apply the Kubernetes manifest:
    *  `$ kubectl apply -f wordsmith-demo.yml`
8.  Run the Kubernetes dashboard:
    *  `$ kubectl proxy`

When you are done, remember to destroy your environment with `terraform destroy`.

You can now access the dashboard at http://localhost:8001/api/v1/namespaces/kube-system/services/kubernetes-dashboard/proxy/#!/overview?namespace=default.  
